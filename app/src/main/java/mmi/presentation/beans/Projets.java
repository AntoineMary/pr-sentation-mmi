package mmi.presentation.beans;

/**
 * Created by Skynniman on 08/03/2015.
 */
public class Projets {
    private int annee = 0;
    private int annee_etude = 0;
    private String description = "";
    private String image = "";
    private String nom = "";
    private String nom_projet = "";
    private String prenom = "";
    private int type = 0;
    private String type_projet;

    public String getType_projet() {
        return type_projet;
    }

    public void setType_projet(String type_projet) {
        this.type_projet = type_projet;
    }

    public Projets() {

    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public int getAnnee_etude() {
        return annee_etude;
    }

    public void setAnnee_etude(int annee_etude) {
        this.annee_etude = annee_etude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom_projet() {
        return nom_projet;
    }

    public void setNom_projet(String nom_projet) {
        this.nom_projet = nom_projet;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
