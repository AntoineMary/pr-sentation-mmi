package mmi.presentation.beans;

/**
 * Created by Skynniman on 07/03/2015.
 */
public class Cours {
    private String description = "";
    private String icone = "";
    private String nom = "";
    private String type = "";

    public Cours() {
    }

    public String getDescription() {
        return description;
    }
    public String getIcone() {
        return icone;
    }
    public String getNom() {
        return nom;
    }
    public String getType() {
        return type;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public void setIcone(String icone) {
        this.icone = icone;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setType(String type) {
        this.type = type;
    }









}
