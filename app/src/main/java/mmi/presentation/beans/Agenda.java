package mmi.presentation.beans;

/**
 * Created by Skynniman on 08/03/2015.
 */
public class Agenda {

    private String date;
    private String description;
    private int image;
    private String nom;
    private String nom_court;

    public Agenda() {

    }

    public String getNom_court() {
        return nom_court;
    }

    public void setNom_court(String nom_court) {
        this.nom_court = nom_court;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
