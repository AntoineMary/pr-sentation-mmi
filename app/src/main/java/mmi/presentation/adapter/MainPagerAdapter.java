package mmi.presentation.adapter;

/**
 * Created by Skynniman on 05/03/2015.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import mmi.presentation.R;
import mmi.presentation.fragment.*;

import java.util.Locale;

/**
 * A {@link android.support.v4.app.FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public MainPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 : return AccueilFragment.newInstance();
            case 1 : return MmiFragment.newInstance();
            case 2 : return GeolocalisationFragment.newInstance();
            case 3 : return AgendaFragment.newInstance();
            case 4 : return ActualitesFragment.newInstance();
            default: return AccueilFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return context.getString(R.string.title_fragment_accueil).toUpperCase(l);
            case 1:
                return context.getString(R.string.title_fragment_mmi).toUpperCase(l);
            case 2:
                return context.getString(R.string.title_fragment_geolocalisation).toUpperCase(l);
            case 3:
                return context.getString(R.string.title_fragment_agenda).toUpperCase(l);
            case 4:
                return context.getString(R.string.title_fragment_actualite).toUpperCase(l);

        }
        return null;
    }
}
