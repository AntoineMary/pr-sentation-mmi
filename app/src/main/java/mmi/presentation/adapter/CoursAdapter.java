package mmi.presentation.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import mmi.presentation.R;
import mmi.presentation.activity.MainActivity;
import mmi.presentation.beans.Cours;

/**
 * Created by Skynniman on 07/03/2015.
 */
public class CoursAdapter extends ArrayAdapter<Cours> {

    private Context context;

    public CoursAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result = convertView;

        if (convertView == null) {
            result = LayoutInflater.from(context).inflate(R.layout.ligne_cours, parent, false);
        }

        Cours cour = getItem(position);
        int iconeID = context.getResources().getIdentifier(context.getPackageName() + ":string/"+ cour.getIcone(), null, null);

        TextView titre =        (TextView) result.findViewById(R.id.titre_cours);
        TextView icone =        (TextView) result.findViewById(R.id.icone_cours);
        TextView description =  (TextView) result.findViewById(R.id.description_cours);

        titre.setText(cour.getNom());
        icone.setText(context.getString(iconeID));
        icone.setTypeface(MainActivity.fontawesome);
        description.setText(cour.getDescription());

        if (cour.getType().equals("graphisme")) {
            titre.setTextColor(Color.BLACK);
            icone.setTextColor(Color.BLACK);
            description.setTextColor(Color.BLACK);
        }


        return result;
    }

    public void updateData(){
        this.notifyDataSetChanged();
    }
}
