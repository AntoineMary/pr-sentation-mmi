package mmi.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import mmi.presentation.R;
import mmi.presentation.beans.Agenda;

/**
 * Created by Skynniman on 08/03/2015.
 */
public class AgendaAdapter extends ArrayAdapter<Agenda> {

    private Context context;

    public AgendaAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View result = convertView;

        if (convertView == null) {
            result = LayoutInflater.from(getContext()).inflate(R.layout.ligne_agenda, parent, false);
        }

        Agenda agenda = getItem(position);

        TextView nom_court = (TextView) result.findViewById(R.id.nom_court);
        nom_court.setText(agenda.getNom_court());

        TextView date = (TextView) result.findViewById(R.id.date);
        date.setText(agenda.getDate());

        TextView nom = (TextView) result.findViewById(R.id.nom);
        nom.setText(agenda.getNom());

        TextView description = (TextView) result.findViewById(R.id.description);
        description.setText(agenda.getDescription());

        ImageView image = (ImageView) result.findViewById(R.id.image);
        image.setImageResource(agenda.getImage());

        return result;
    }
    public void updateData(){
        this.notifyDataSetChanged();
    }
}