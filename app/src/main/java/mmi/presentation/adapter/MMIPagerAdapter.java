package mmi.presentation.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import mmi.presentation.R;
import mmi.presentation.fragment.*;

import java.util.Locale;

/**
 * Created by Skynniman on 07/03/2015.
 */
public class MMIPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public MMIPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 : return CoursFragment.newInstance();
            case 1 : return ProjetsFragment.newInstance();
            case 2 : return StageFragment.newInstance();
            default: return CoursFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return context.getString(R.string.title_fragment_cours).toUpperCase(l);
            case 1:
                return context.getString(R.string.title_fragment_projets).toUpperCase(l);
            case 2:
                return context.getString(R.string.title_fragment_stage).toUpperCase(l);

        }
        return null;
    }
}
