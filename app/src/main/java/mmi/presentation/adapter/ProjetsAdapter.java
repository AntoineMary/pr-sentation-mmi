package mmi.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import mmi.presentation.R;
import mmi.presentation.beans.Projets;

import java.util.ArrayList;

/**
 * Created by Skynniman on 08/03/2015.
 */
public class ProjetsAdapter extends ArrayAdapter<Projets> {

    private Context context;

    public ProjetsAdapter(Context context, ArrayList<Projets> projets) {
        super(context, 0, projets);
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (getItem(position) != null) {
            Projets projet1 = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.ligne_projet_100, parent, false);
            }

            TextView titre = (TextView) convertView.findViewById(R.id.titre);
            TextView contexte = (TextView) convertView.findViewById(R.id.contexte);

            if (titre != null) {
                titre.setText(projet1.getNom_projet());
            }

            if (contexte != null) {
                contexte.setText(projet1.getType_projet());
            }


        }
        return convertView;
    }

}
