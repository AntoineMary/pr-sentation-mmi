package mmi.presentation.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import mmi.presentation.R;
import mmi.presentation.activity.PresentationActivity;
import mmi.presentation.beans.Actualites;

import java.util.ArrayList;

/**
 * Created by Skynniman on 08/03/2015.
 */
public class ActualitesAdapter extends ArrayAdapter<Actualites> {

    private Context context;

    public ActualitesAdapter(Context context, ArrayList<Actualites> actualites) {
        super(context, 0, actualites);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Actualites actualite = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.ligne_actualite, parent, false);
        }

        TextView titre = (TextView) convertView.findViewById(R.id.titre);
        titre.setText(actualite.getNom());
        TextView contexte = (TextView) convertView.findViewById(R.id.contexte);
        contexte.setText(actualite.getDate());

        LinearLayout conteneur = (LinearLayout) convertView.findViewById(R.id.conteneur);
        if(actualite.getImage() != 0) {
            conteneur.setBackgroundResource(actualite.getImage());
        }

        final String titreintent = actualite.getNom();
        final String contexteintent = actualite.getDate();
        final int fondintent = actualite.getImage();
        final String descripptionintent = actualite.getDescription();

        View.OnClickListener activitylaucher = new View.OnClickListener(){
            public void onClick(View arg0) {
                Intent intent = new Intent(context, PresentationActivity.class);
                intent.putExtra("titre", titreintent);
                intent.putExtra("contexte", contexteintent);
                intent.putExtra("fond", ""+fondintent);
                intent.putExtra("description", descripptionintent);
                context.startActivity(intent);

            }
        };

        conteneur.setOnClickListener(activitylaucher);
        titre.setOnClickListener(activitylaucher);
        contexte.setOnClickListener(activitylaucher);

        return convertView;
    }
}
