package mmi.presentation.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import mmi.presentation.R;

public class SplashActivity extends Activity {

    private static final int SPLASH_STOP = 1;
    private static final long SPLASH_TIME = 3000;

    private Handler splashHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SPLASH_STOP:
                    // Lancement de l'activité Main par Intent
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    // Fin du handler
                    finish();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        Message msg = new Message();
        // La méthode what définit le message à envoyer
        msg.what = SPLASH_STOP;
        // Délai de réception du message par le handler
        splashHandler.sendMessageDelayed(msg, SPLASH_TIME);


    }
}
