package mmi.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.LinearLayout;
import android.widget.TextView;
import mmi.presentation.R;


public class PresentationActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentation);

        Intent intent = getIntent();

        String titreintent = intent.getStringExtra("titre");
        String contexteintent = intent.getStringExtra("contexte");
        String fondintent = intent.getStringExtra("fond");
        String descintent = intent.getStringExtra("description");

        TextView titre = (TextView) findViewById(R.id.titre);
        titre.setText(titreintent);
        TextView contexte = (TextView) findViewById(R.id.contexte);
        contexte.setText(contexteintent);
        TextView desc = (TextView) findViewById(R.id.desc);
        desc.setText(descintent);

        LinearLayout conteneur = (LinearLayout) findViewById(R.id.conteneur);
        if(!fondintent.equals("0")) {
            conteneur.setBackgroundResource(Integer.parseInt(fondintent));
        }
    }


}
