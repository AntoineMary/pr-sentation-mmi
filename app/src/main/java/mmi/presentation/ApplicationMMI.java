package mmi.presentation;

import android.app.Application;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import mmi.presentation.util.FontsOverride;

/**
 * Created by Skynniman on 07/03/2015.
 */
public class ApplicationMMI extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "alFO4F2LVh35zZwINMSRJkCNw";
    private static final String TWITTER_SECRET = "rs8Gcxe3mbjFtn7WrIKaSUrppIZowE339dD82Z0Y4l6KmqboJa";



    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        FontsOverride.setDefaultFont(this, "SERIF", "DroidSerif-Regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "Montserrat-Regular.otf");

    }
}
