package mmi.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.linearlistview.LinearListView;
import mmi.presentation.R;
import mmi.presentation.adapter.AgendaAdapter;
import mmi.presentation.beans.Agenda;
import org.xmlpull.v1.XmlPullParser;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AgendaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgendaFragment extends Fragment {

    public static AgendaFragment newInstance() {
        return new AgendaFragment();
    }

    public AgendaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View agendaView = inflater.inflate(R.layout.fragment_agenda, container, false);

        LinearListView listeAgenda = (LinearListView) agendaView.findViewById(R.id.listeAgenda);
        AgendaAdapter agendaAdapter = new AgendaAdapter(getActivity(), R.layout.ligne_agenda);

        try {
            XmlPullParser xmlPullParser = getResources().getXml(R.xml.agenda);


            while (xmlPullParser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (xmlPullParser.getEventType() == XmlPullParser.START_TAG) {
                    if (xmlPullParser.getName().equals("evenement")) {
                        Agenda agenda = new Agenda();

                        agenda.setDate(xmlPullParser.getAttributeValue(0));
                        agenda.setDescription(xmlPullParser.getAttributeValue(1));
                        String path = getActivity().getPackageName() + ":drawable/" + xmlPullParser.getAttributeValue(2);
                        int resID = getResources().getIdentifier(path, null, null);
                        agenda.setImage(resID);
                        agenda.setNom(xmlPullParser.getAttributeValue(3));
                        agenda.setNom_court(xmlPullParser.getAttributeValue(4));

                        agendaAdapter.add(agenda);
                    }
                }
                xmlPullParser.next();
            }
            listeAgenda.setAdapter(agendaAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return agendaView;
    }

}
