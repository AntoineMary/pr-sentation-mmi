package mmi.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.astuetz.PagerSlidingTabStrip;
import mmi.presentation.R;
import mmi.presentation.adapter.MMIPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 * to handle interaction events.
 * Use the {@link MmiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MmiFragment extends Fragment {


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    public static MMIPagerAdapter mMMIPagerAdapter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    PagerSlidingTabStrip tabsStrip;



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MmiFragment.
     */
    public static MmiFragment newInstance() {
        return new MmiFragment();
    }

    public MmiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMMIPagerAdapter = new MMIPagerAdapter(getChildFragmentManager(), getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View MMIview = inflater.inflate(R.layout.fragment_mmi, container, false);
        // Inflate the layout for this fragment


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) MMIview.findViewById(R.id.pagerMMI);
        mViewPager.setAdapter(mMMIPagerAdapter);
        mViewPager.setOffscreenPageLimit(0);

        tabsStrip = (PagerSlidingTabStrip) MMIview.findViewById(R.id.tabsMMI);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(mViewPager);

        return MMIview;
    }

}
