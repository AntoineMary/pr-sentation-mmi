package mmi.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.linearlistview.LinearListView;
import mmi.presentation.R;
import mmi.presentation.adapter.CoursAdapter;
import mmi.presentation.adapter.ProjetsAdapter;
import mmi.presentation.beans.Cours;
import mmi.presentation.beans.Projets;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProjetsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProjetsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProjetsFragment extends Fragment {

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProjetsFragment.
     */
    public static ProjetsFragment newInstance() {
        return new ProjetsFragment();
    }

    public ProjetsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View projetsView = inflater.inflate(R.layout.fragment_projets, container, false);
        final LinearListView listeProjet1 = (LinearListView) projetsView.findViewById(R.id.projet_1);
        final LinearListView listeProjet2 = (LinearListView) projetsView.findViewById(R.id.projet_2);

        try {
            XmlPullParser xmlPullParser = getResources().getXml(R.xml.projets);

            ArrayList<Projets> projets1 = new ArrayList<Projets>();
            ArrayList<Projets> projets2 = new ArrayList<Projets>();

            while (xmlPullParser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (xmlPullParser.getEventType() == XmlPullParser.START_TAG) {
                    if (xmlPullParser.getName().equals("projet")) {
                        Projets projet = new Projets();

                        projet.setAnnee(Integer.parseInt(xmlPullParser.getAttributeValue(0)));
                        projet.setAnnee_etude(Integer.parseInt(xmlPullParser.getAttributeValue(1)));
                        projet.setDescription(xmlPullParser.getAttributeValue(2));
                        projet.setImage(xmlPullParser.getAttributeValue(3));
                        projet.setNom(xmlPullParser.getAttributeValue(4));
                        projet.setNom_projet(xmlPullParser.getAttributeValue(5));
                        projet.setPrenom(xmlPullParser.getAttributeValue(6));
                        projet.setType(Integer.parseInt(xmlPullParser.getAttributeValue(7)));
                        projet.setType_projet(xmlPullParser.getAttributeValue(8));


                        if (projet.getAnnee_etude() == 1 ) { projets1.add(projet); }
                        else if (projet.getAnnee_etude() == 2) { projets2.add(projet); }
                        else { Log.w("Projet en erreur :", projet.getNom()); }
                    }
                }
                xmlPullParser.next();
            }

            ProjetsAdapter ProjetAdapter1 = new ProjetsAdapter(getActivity(), projets1);
            listeProjet1.setAdapter(ProjetAdapter1);
            ProjetsAdapter ProjetAdapter2 = new ProjetsAdapter(getActivity(), projets2);
            listeProjet2.setAdapter(ProjetAdapter2);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return projetsView;
    }

}
