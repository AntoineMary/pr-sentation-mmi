package mmi.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.linearlistview.LinearListView;
import mmi.presentation.R;
import mmi.presentation.adapter.CoursAdapter;
import mmi.presentation.beans.Cours;
import org.xmlpull.v1.XmlPullParser;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CoursFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CoursFragment extends Fragment {

    private Boolean render = false;

    public CoursFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CoursFragment.
     */

    public static CoursFragment newInstance() {
        CoursFragment fragment = new CoursFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View coursView = inflater.inflate(R.layout.fragment_cours, container, false);

        final LinearListView listeCoursCom = (LinearListView) coursView.findViewById(R.id.list_cours_com);
        final LinearListView listeCoursGraph = (LinearListView) coursView.findViewById(R.id.list_cours_graphisme);
        final LinearListView listeCoursDev = (LinearListView) coursView.findViewById(R.id.list_cours_developpement);

        CoursAdapter coursAdapterCom = new CoursAdapter(getActivity(), R.layout.ligne_cours);
        CoursAdapter coursAdapterGraph = new CoursAdapter(getActivity(), R.layout.ligne_cours);
        CoursAdapter coursAdapterDev = new CoursAdapter(getActivity(), R.layout.ligne_cours);

        try {
            XmlPullParser xmlPullParser = getResources().getXml(R.xml.cours);

            listeCoursCom.setAdapter(coursAdapterCom);
            listeCoursGraph.setAdapter(coursAdapterGraph);
            listeCoursDev.setAdapter(coursAdapterDev);

            while (xmlPullParser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (xmlPullParser.getEventType() == XmlPullParser.START_TAG) {
                    if (xmlPullParser.getName().equals("cour")) {
                        Cours cour = new Cours();

                        cour.setDescription(xmlPullParser.getAttributeValue(0));
                        cour.setIcone(xmlPullParser.getAttributeValue(1));
                        cour.setNom(xmlPullParser.getAttributeValue(2));
                        cour.setType(xmlPullParser.getAttributeValue(3));

                        if (cour.getType().equals("communication")) { coursAdapterCom.add(cour); }
                        else if (cour.getType().equals("graphisme")) { coursAdapterGraph.add(cour); }
                        else if (cour.getType().equals("developpement")) {coursAdapterDev.add(cour); }
                        else { Log.w("Cours en erreur :", cour.getNom()); }
                    }
                }
                xmlPullParser.next();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return coursView;
    }

}
