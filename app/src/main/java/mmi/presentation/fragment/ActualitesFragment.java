package mmi.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.linearlistview.LinearListView;
import mmi.presentation.R;
import mmi.presentation.adapter.ActualitesAdapter;
import mmi.presentation.beans.Actualites;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ActualitesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActualitesFragment extends Fragment {

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ActualitesFragment.
     */
    public static ActualitesFragment newInstance() {
        return new ActualitesFragment();
    }

    public ActualitesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View actuView = inflater.inflate(R.layout.fragment_actualites, container, false);
        final LinearListView listeActu = (LinearListView) actuView.findViewById(R.id.actus);

        try {
            XmlPullParser xmlPullParser = getResources().getXml(R.xml.actualite);

            ArrayList<Actualites> actus = new ArrayList<Actualites>();

            while (xmlPullParser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (xmlPullParser.getEventType() == XmlPullParser.START_TAG) {
                    if (xmlPullParser.getName().equals("evenement")) {
                        Actualites actu = new Actualites();

                        actu.setDate(xmlPullParser.getAttributeValue(0));
                        actu.setDescription(xmlPullParser.getAttributeValue(1));
                        String path = getActivity().getPackageName() + ":drawable/" + xmlPullParser.getAttributeValue(2);
                        int resID = getResources().getIdentifier(path, null, null);
                        actu.setImage(resID);
                        actu.setNom(xmlPullParser.getAttributeValue(3));

                        actus.add(actu);

                    }
                }
                xmlPullParser.next();
            }

            ActualitesAdapter actualitesAdapter = new ActualitesAdapter(getActivity(), actus);
            listeActu.setAdapter(actualitesAdapter);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return actuView;
    }

}
